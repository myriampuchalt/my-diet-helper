<?php
class adminModel{

	protected $connection;

	public function adminModel($DB_name, $DB_user, $DB_password, $DB_hostname){

		$DB_connection =  mysqli_connect($DB_hostname, $DB_user, $DB_password);

		if (!$DB_connection){
			die('It has not been possible to connect with the DB: ' . mysql_error());
		}

		mysqli_select_db($DB_connection, $DB_name);
		$this->connection = $DB_connection;
	}
	
	public function insertArticle($title, $content){
		$title = mysqli_real_escape_string($this->connection, $title);
		$content = mysqli_real_escape_string($this->connection, $content);
		$sql = "INSERT INTO articles (title, content) values('$title', '$content')";
		$result = mysqli_query($this->connection,$sql);
	}
	
	public function getArticles(){
		$sql = "SELECT * FROM articles ORDER BY id ASC";
		$result = mysqli_query($this->connection, $sql);
		
		$articles = array();
		while ($row = mysqli_fetch_assoc($result)){
			$articles[] = $row;
		}
		return $articles;
		
	}
	public function getUsers(){
		$sql = "SELECT * FROM users ORDER BY id ASC";
		$result = mysqli_query($this->connection, $sql);
		
		$users = array();
		while ($row = mysqli_fetch_assoc($result)){
			$users[] = $row;
		}
		return $users;
	}
	
	public function deleteUser($id){
		$sql = "DELETE FROM users WHERE id=".$id;
		$result = mysqli_query($this->connection,$sql);
	}
	
	public function deleteArticle($id){
		$sql = "DELETE FROM articles WHERE id=".$id;
		$result = mysqli_query($this->connection,$sql);
	}
	
	public function validateNumber($number){
		$pattern = '/[0-9]+/';
		if (preg_match($pattern, $number)) return true;
		else return false;
	}
	
}
?>