<?php

class adminController{
	
	public $adminModel;
	
	public function adminController(){
		$this->adminModel = new adminModel(Config::$DB_name, Config::$DB_user, Config::$DB_password, Config::$DB_hostname);
	}
	
	// Login-Logout Functions
	
	public function adminHome(){
		require __DIR__.'/views/adminHome.php';
	}
	
	
	
	public function insertArticle(){
		
		$params = array(
				'id' => '',
				'result' => array(),
		);
		$params['result'] = $this->adminModel->getArticles();
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$this->adminModel->insertArticle($_POST['title'], $_POST['content']);
			header('Location: index.php?ctl=insertArticle');
		}
		require __DIR__.'/views/insertArticle.php';
	}
	
	public function deleteArticle(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if ($this->adminModel -> validateNumber($_POST['articleId'])){
				
				$params['id'] = $_POST['articleId'];
				$this->adminModel->deleteArticle($params['id']);
				$params['message'] = "Article successfully deleted";
				header('Location: index.php?ctl=insertArticle');
			} else $params['message'] = "Please insert a valid Id";
		}
			
		
		require __DIR__.'/views/insertArticle.php';
	}
	
	public function listUsers(){
		
		$params = array(
				'id' => '',
				'result' => array(),
		);
		$params['result'] = $this->adminModel->getUsers();
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if ($this->adminModel -> validateNumber($_POST['userId'])){
				$params['id'] = $_POST['userId'];
				$this->adminModel->deleteUser($params['id']);
				$params['message'] = "User successfully deleted";
				header('Location: index.php?ctl=listUsers');
			} else $params['message'] = "Please insert a valid Id";
		}
			
		
		require __DIR__.'/views/listUsers.php';
	}
	
	
}

?>