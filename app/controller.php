<?php
class Controller{
	public $model;
	
	// Constructor
	
	public function Controller(){
		$this->model = new Model(Config::$db, Config::$user, Config::$password, Config::$host);
	}
	
	// Sign Up Function
	
	public function signUp(){
	
		$params = array(
				'username' => '',
				'password' => '',
				'email' => '',
		);
	
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			if (empty($_POST['username']) || empty($_POST['password']) || empty($_POST['email'])){
				$params['message'] = "Please enter valid information. Some gaps are empty";
			} else{
				if (($this->model->validateData ($_POST ['username'] )) && ($this->model->validateData ($_POST ['password'] )) && ($this->model->validateEmail ( $_POST ['email'] ))) {
					// check if user exists in de DB
					if (! ($this->model->userExists ( $_POST ['username'] ))) {
						
						$this->model->createUser ( $_POST ['username'], $_POST ['password'], $_POST ['email']);
						$params ['message'] = 'User successfully created! Please Log In now';
					} else {
						$params ['message'] = 'Username already exists. Please try again';
					}
				} else {
					$params ['message'] = 'Please provide valid information and try again';
				}
			}				
		}
		require __DIR__ . '/views/login.php';
	}
	
	// Login-Logout Functions
	
	public function login() {
		$params = array (
				'username' => '',
				'password' => '' 
		);
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			if ($this->model->validateData ( $_POST ['username'], $_POST ['password'] )) {
				// check if user exists in de DB
				if ($this->model->userExists ( $_POST ['username'], $_POST ['password'] )) {
					$_SESSION ['name'] = $_POST ['username'];
					$user = $this->model->getUser ( $_SESSION ['name'] );
					$_SESSION['id'] = $user['id'];
					$profilePic = $this->model -> getProfilePic($_SESSION['id']);
					if ($profilePic !== "") $_SESSION['profilePic'] = $profilePic;
					setcookie ( "LastConexion", date ( "H.i.s", time () ), time () + 315360000 );
					
					if ($_SESSION ['name'] == "admin") {
						$_SESSION['admin'] == true;
						header ( 'Location: index.php?ctl=adminHome' );
					} else {
						if (! (isset ( $_COOKIE ['nOfLogins'] ))) {
							setcookie ( "nOfLogins", 1, time () + 315360000 ); // The Cookies will expire in 1 Year
							header ( 'Location: index.php?ctl=home' );
						} else {
							setcookie ( "nOfLogins", $_COOKIE ['nOfLogins'] + 1, time () + 315360000 );
							
							if ($_COOKIE ['nOfLogins'] % 5 === 0) {
								$articlePosition = $_COOKIE ['nOfLogins'] / 5 - 1; // because the first position of an array is 0
								$this->sendArticle ( $user, $articlePosition );
								header ( 'Location: index.php?ctl=home&showMessage=true' );
							} else {
								header ( 'Location: index.php?ctl=home' );
							}
						}
						
					}
				} else {
					$params = array (
							'username' => $_POST ['username'],
							'password' => $_POST ['password'] 
					);
					$params ['message'] = 'Incorrect username/password. Please try again';
				}
			}
		}
		require __DIR__ . '/views/login.php';
	}
	
	public function logout(){
		session_start(); // to ensure you are using the same session
		session_destroy(); // destroy the session
		header ('Location: index.php?ctl=login');
	}
	
	// Home
	
	public function home(){
		
		$user = $this->model->getUser ( $_SESSION ['name'] );
		
		$params = array(
				'age' => $user["age"],
				'weight' => $user['weight'],
				'height' => $user['height'],
				'goalWeight' => $user['goalWeight'],
				'goalKcal' => $user['goalKcal'],
		);
		require __DIR__.'/views/home.php';
	}
	
	// Insert into DB
		
	public function insertPI(){
		
		$params = array(
				'age' => '',
				'weight' => '',
				'height' => '',
				'goalWeight' => '',
				'goalKcal' => '',
		);
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			// First we check that the form data is correct
			if ($this->model -> validateNumber($_POST['age']) && $this->model -> validateNumber($_POST['weight']) && $this->model -> validateNumber($_POST['height'])
					&& $this->model -> validateNumber($_POST['goalWeight']) && $this->model -> validateNumber($_POST['goalKcal'])){
				$this->model -> insertPI($_SESSION['name'], $_POST['age'], $_POST['weight'], $_POST['height'], $_POST['goalWeight'], $_POST['goalKcal']);
				header('Location: index.php?ctl=home');
			} else{
				$params = array(
						'age' => $_POST['age'],
						'weight' => $_POST['weight'],
						'height' => $_POST['height'],
						'goalWeight' => $_POST['goalWeight'],
						'goalKcal' => $_POST['goalKcal'],
				);
				$params['message'] = 'Please insert valid numbers';
			}
	}
	require __DIR__.'/views/home.php';
	}
	
	public function insertWeight(){
		
		
		$params = array(
			'date' => '',
			'weight' => '',
			'result' => array(),
		);
		
		$params['result'] = $this->model -> getWeights($_SESSION['id']);
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			// First we check that the form data is correct
			if ($this->model -> validateNumber($_POST['weight'])){
				$this->model -> insertWeight($_SESSION['id'], $_POST['date'], $_POST['weight']);
				header('Location: index.php?ctl=insertWeight');
			} else{
				$params = array(
						'date' => $_POST['date'],
						'weight' => $_POST['weight'],
				);
				$params['message'] = 'Please insert a valid weight';
				
			}
			
		}
		require __DIR__.'/views/insertWeight.php';
	}
	
	public function insertMeasurements(){
	
		$params = array(
				'date' => '',
				'arm' => '',
				'waist' => '',
				'hips' => '',
				'leg' => '',
				'result' => array(),
		);
		
		$params['result'] = $this->model -> getMeasurements($_SESSION['id']);
	
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			// First we check that the form data is correct
			if ($this->model -> validateNumber($_POST['arm']) && $this->model -> validateNumber($_POST['waist']) && $this->model -> validateNumber($_POST['hips'])
					&& $this->model -> validateNumber($_POST['leg'])){
				$this->model -> insertMeasurements($_SESSION['id'], $_POST['date'], $_POST['arm'], $_POST['waist'], $_POST['hips'], $_POST['leg']);
				header('Location: index.php?ctl=insertMeasurements');
			} else{
				$params = array(
						'date' => $_POST['date'],
						'arm' => $_POST['arm'],
						'waist' => $_POST['waist'],
						'hips' => $_POST['hips'],
						'leg' => $_POST['leg'],
				);
				$params['message'] = 'Please insert valid measurements';
			}
			
		}
	
		require __DIR__.'/views/insertMeasurements.php';
	
	}
	
	
	public function insertWorkout(){
		
		$params = array(
				'date' => '',
				'name' => '',
				'kcal' => '',
				'time' => '',
				'result' => array(),
		);
		
		$params['result'] = $this->model -> getWorkouts($_SESSION['id']);
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			// First we check that the form data is correct
			if ($this->model -> validateNumber($_POST['kcal']) && $this->model -> validateNumber($_POST['time'])){
				$this->model -> insertWorkout($_SESSION['id'], $_POST['date'], $_POST['name'], $_POST['kcal'], $_POST['time']);
				header('Location: index.php?ctl=insertWorkout');
			} else{
				$params = array(
						'date' => $_POST['date'],
						'name' => $_POST['name'],
						'kcal' => $_POST['kcal'],
						'time' => $_POST['time'],
				);
				$params['message'] = 'Please insert valid Calories & Minutes';
			}
			
		}
		require __DIR__.'/views/insertWorkout.php';
	
	}
	
	public function insertProfilePic() {
		
		$user = $this->model->getUser ( $_SESSION ['name'] );
		
		$params = array(
				'age' => $user["age"],
				'weight' => $user['weight'],
				'height' => $user['height'],
				'goalWeight' => $user['goalWeight'],
				'goalKcal' => $user['goalKcal'],
		);
		
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			
			if (isset ( $_FILES ['image'] ) && ! empty ( $_FILES ['image'] ['name'] )) {
				if (is_uploaded_file ( $_FILES ['image'] ['tmp_name'] )) {
					$size = $_FILES ['image'] ['size'];
					if ($size > 64000) {
						$params ['message'] = "The image can't be bigger than 64kb";
					} else{
						$image = addslashes(file_get_contents($_FILES ['image'] ['tmp_name']));
						$this->model -> insertProfilePic($_SESSION['id'], $image);
						$_SESSION['profilePic'] = $this->model -> getProfilePic($_SESSION['id']);
						$params ['message'] = "Picture successfully uploaded";
						header('Location: index.php?ctl=home');
					}
				}
			}
		}
		require __DIR__ . '/views/home.php';
	}
	
	public function insertImage() {
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			if (isset ( $_FILES ['image'] ) && ! empty ( $_FILES ['image'] ['name'] )) {
				if (is_uploaded_file ( $_FILES ['image'] ['tmp_name'] )) {
					$name = $_FILES ['image'] ['name'];
					$type = $_FILES ['image'] ['type'];
					$size = $_FILES ['image'] ['size'];
					if (($type !== "image/jpeg") && ($type !== "image/png")) {
						$params ['message'] = "The image needs to be jpg or png";
					} else {
						if ($size > 400000) {
							$params ['message'] = "The image can't be bigger than 400kb";
						} else {
							$directory = "../web/img/" . $_SESSION ['name'] . "/";
							if (! is_dir ( $directory )) {
								mkdir ( $directory, 0777, true );
							} 
							move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $directory . $name );
							
						}
					}
				}
			}
		}
		require __DIR__ . '/views/insertImage.php';
	}
	
	// Get from DB certain DATE
	
	public function getWeight(){

		$params = array(
				'date' => '',
				'result' => array(),
		);
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$params['date'] = $_POST['date'];
			$params['result'] = $this->model -> getWeights($_SESSION['id'], $_POST['date']);
		}
		
		require __DIR__.'/views/insertWeight.php';
	}
	
	public function getMeasurements(){
	
		$params = array(
				'date' => '',
				'result' => array(),
		);
	
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$user = $_SESSION['name'];
			$params['date'] = $_POST['date'];
			$params['result'] = $this->model -> getMeasurements($_SESSION['id'], $_POST['date']);
		}
	
		require __DIR__.'/views/insertMeasurements.php';
	
	}
	
	public function getWorkout(){
		
		$params = array(
				'date' => '',
				'result' => array(),
		);
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$params['date'] = $_POST['date'];
			$params['result'] = $this->model -> getWorkout($_POST['date'], $_SESSION['id']);
		}
		
		require __DIR__.'/views/insertWorkout.php';
		
	}
	
	// Send Email to User
	
	public function sendArticle($user, $articlePosition){
	
		$article = $this->model -> getArticle($articlePosition);
		$mail_sent = mail($user[0]['email'],"MyDietHelper Fitness Article", $article);
		if ($mail_sent == true) {
			$params['message'] = 'We have succesfully sent you an email. Enjoy reading!';
		} else{
			$params['message'] = 'Oops, it has been impossible for us to send you an email. Please check your email on your account information.';
		}
	}
	
}
