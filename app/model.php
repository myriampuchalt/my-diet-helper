<?php

class Model{
	
	protected $connection;
	
	// Constructor
	
    public function Model($db, $user, $password, $host) {
	    
        try {
          	$connection = new PDO("mysql:host=$host;dbname=$db;charset=utf8",$user,$password);  
        } catch (PDOException $err) {  
            echo "The connection to mysql server failed";
            $err->getMessage() . "<br/>";
            file_put_contents('PDOErrors.txt',$err, FILE_APPEND);  // write some details to an error-log outside public_html  
            die();  //  terminate connection
        }
    }
		
	// Validation Functions
	
	public function userExists($username){
	    
	    $sql = "SELECT count(*) FROM users WHERE username= :username";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->execute();
		
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return ($result != 0);
		
		$connection = NULL;		
	}
	
	public function validateData($data){
	    
		$pattern = '/[a-zA-Z0-9_-]{3,15}$/';
		return preg_match($pattern, $data);
		
	}
	
	public function validateEmail($email){
	    
		$pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
		return preg_match($pattern, $email); 
	}
	
	public function validateNumber($number){
	    
		$pattern = '/[0-9]+/';
		return preg_match($pattern, $number); 
		
	}
	
	// Create new User
	
	public function createUser($username, $password, $email){
	    
	    $sql = "INSERT INTO users (username, password, email) values( :usr, :psswd, :mail)";
	    $stmt = $connection->prepare($sql);
	    $stmt->bindParam(':usr', $username, PDO::PARAM_STR);
	    $stmt->bindParam(':psswd', $password, PDO::PARAM_STR);
	    $stmt->bindParam(':mail', $email, PDO::PARAM_STR);
		$stmt->execute();
		
		$connection = NULL;
	
	}
	
	// Insert into DB
	
	public function insertPI($name, $age, $weight, $height, $goalWeight, $goalKcal){
	    
	    $sql = "UPDATE users SET age= :age, weight= :weight, height= :height, goalWeight= :goalW, goalKcal= :goalK WHERE username= :name";
	    $stmt = $connection->prepare($sql);
	    $stmt->bindParam(':age', $age, PDO::PARAM_INT);
	    $stmt->bindParam(':weight', $weight, PDO::PARAM_INT);
	    $stmt->bindParam(':height', $height, PDO::PARAM_INT);
	    $stmt->bindParam(':goalW', $goalWeight, PDO::PARAM_INT);
	    $stmt->bindParam(':goalK', $goalKcal, PDO::PARAM_INT);
	    $stmt->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt->execute();
		
		$connection = NULL;
	}
	
	public function insertWeight($id, $date, $weight){
	    
		$params['message'] = var_dump($id);
		
        $sql = "INSERT INTO weight (userID, kg, date)";
        $sql .= "VALUES (:id, :weight, :date)";
	    $stmt = $connection->prepare($sql);
	    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	    $stmt->bindParam(':weight', $weight, PDO::PARAM_INT);
	    $stmt->bindParam(':date', date("Y-m-d", strtotime($date)), PDO::PARAM_STR);
		$stmt->execute();
		
		$connection = NULL;
	}
	
	public function insertMeasurements($id, $date, $arm, $waist, $hips, $leg){

		$sql = "INSERT INTO measurements (userID, date, arm, waist, hips, leg)";
		$sql .= "VALUES (:id, :date, :arm, :waist, :hips, :leg)";
		$stmt = $connection->prepare($sql);
	    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	    $stmt->bindParam(':date', date("Y-m-d", strtotime($date)), PDO::PARAM_STR);
	    $stmt->bindParam(':arm', $arm, PDO::PARAM_INT);
	    $stmt->bindParam(':waist', $waist, PDO::PARAM_INT);
	    $stmt->bindParam(':hips', $hips, PDO::PARAM_INT);
	    $stmt->bindParam(':leg', $leg, PDO::PARAM_INT);
		$stmt->execute();
		
		$connection = NULL;
	}
	
	public function insertWorkout($id, $date, $name, $kcal, $time){
	    
	    $sql = "INSERT INTO workouts (userID, date, name, time, kcal)";
		$sql .= "VALUES (:id, :date, :name, :time, :kcal)";
		$stmt = $connection->prepare($sql);
	    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	    $stmt->bindParam(':date', date("Y-m-d", strtotime($date)), PDO::PARAM_STR);
	    $stmt->bindParam(':name', $arm, PDO::PARAM_STR);
	    $stmt->bindParam(':time', $waist, PDO::PARAM_INT);
	    $stmt->bindParam(':kcal', $hips, PDO::PARAM_INT);
		$stmt->execute();
		
		$connection = NULL;
		
	}
	
	// Still to DO
	public function insertProfilePic($id, $image){
		$sql = "UPDATE users SET profilePic='".$image."' WHERE id= :id";
		$stmt = $connection->prepare($sql);
	    $stmt->bindParam(':ppic', $image, PDO::PARAM_INT);
	    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		$connection = NULL;
	}
	
	public function getWeights($id, $date = NULL){
		
		$sql = "SELECT * FROM weight";
		$sql .= $date ? "WHERE date= :date AND userID= :id" : "WHERE userID= :id ORDER BY date";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':date', $id, PDO::PARAM_INT); // ** Not sure what type of param is  date
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
		    $weights [] = $result;
		}
		return $weights;
		
		$connection = NULL;
	}
	
	public function getMeasurements($id, $date = NULL){
		
		$sql = "SELECT * FROM measurements";
		$sql .= $date ? "WHERE date= :date AND userID= :id" : "WHERE userID= :id ORDER BY date";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':date', $id, PDO::PARAM_INT); // ** Not sure what type of param is  date
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
		    $measurements [] = $result;
		}
		return $measurements;
		
		$connection = NULL;
	}
	
	public function getWorkouts($id, $date = NULL){
		
		$sql = "SELECT * FROM workouts";
		$sql .= $date ? "WHERE date= :date AND userID= :id" : "WHERE userID= :id ORDER BY date";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':date', $id, PDO::PARAM_INT); // ** Not sure what type of param is  date
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
		    $workouts [] = $result;
		}
		return $workouts;
		
		$connection = NULL;
	}	
	
	public function getUser($username){
		$sql = "SELECT * FROM users WHERE username='".$username."'";
		$result = mysqli_query($this->connection,$sql);
		
		$user = array();
		while ($row = mysqli_fetch_assoc($result)){
			$user[] = $row;
		}
		return $user[0];
	}
	
	public function getArticle($articlePosition){
		$sql = "SELECT title, content FROM articles";
		$result = mysqli_query($this->connection,$sql);
		
		$articles = array();
		while ($row = mysqli_fetch_assoc($result)){
			$articles[] = $row;
		}
		
		// We check if we have enough articles, as if the users logs in 100 times but we just have 20 articles, he wont get any article
		// If this happens, he will always get the last article we insert into the DB
		
		$arraySize = sizeof($articles);
		if ( $arraySize <= $articlePosition) {
			$email = "<html><body><h1>".$articles[$arraySize-1]['title']."</h1><p>".$articles[$arraySize-1]['content']."</p>";
		} else {
			$email = "<html><body><h1>".$articles[$articlePosition]['title']."</h1><p>".$articles[$articlePosition]['content']."</p>";
		}
		return $email;
	}
	
	public function getProfilePic($id){
		$sql = "SELECT profilePic FROM users WHERE id=".$id;
		$result = mysqli_query($this->connection,$sql);
		
		$user = mysqli_fetch_array($result);
		return $user['profilePic'];
		
		
		$sql = "SELECT profilePic FROM users WHERE id= :id";
        $stmt = $connection->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->bindColumn(1, $image, PDO::PARAM_LOB);
        $stmt->fetch(PDO::FETCH_BOUND);
        header("Content-Type: image/jpg");



		$sql = "SELECT * FROM contacts WHERE id= :id";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
		    $user [] = $result;
		}
		return $user [0];
		
		$connection = NULL;
		
	}
	
}
