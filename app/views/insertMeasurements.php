<?php ob_start(); ?>

<?php  

if(isset($params['message'])): ?>
<b><span style="color:red;"> <?php echo $params['message'] ?></span></b>
<?php endif; ?>

<section class="left">

<h1>Check-In</h1>
<form id='measurements'name="insertMeasurements" method="POST" action="index.php?ctl=insertMeasurements"><p>
 <label for="date">Date:</label><input type="date" id="date" name="date" required="required" value="" />
<br>
<table>
    <tr class="firstrow"> 
        <td>Body Parts (cm)</td><td>Today's Entry</td>
        </tr>
        <tr>
        <td><label for="arm">Arm</label></td><td><input type="text" id="arm" name="arm" value=""/></td>
        </tr>
        <tr>
        <td><label for="waist">Waist</label></td><td><input type="text" id="waist" name="waist" required="required" value=""/></td>
        </tr>
        <tr>
        <td><label for="hips">Hips</label></td><td><input type="text" id="hips" name="hips" required="required" value=""/></td>
        </tr>
        <tr>
        <td><label for="leg">Leg</label></td><td><input type="text" id="leg" name="leg" value=""/></td>
        </tr>
        
 </table>
<button type="submit" name="insertMeasurements">Save Measurements</button>
 </form>
 <br><br><br>
 <h1>Look for a specific day: </h1>
 
 <form id='measurements' name="findMeasurements" method="POST" action="index.php?ctl=getMeasurements">
 <p>
	<label for="date">Date:</label>
			<input type="date" name="date"> <br>
			<button type="submit" name="search">Search</button>
		
</form>

</section>

<section class="right">

<?php if (isset($params['result'])){
$measurements = $params['result']; ?>

<table>
<tr>
<th class="results">Date</th>
<th class="results">Arm</th>
<th class="results">Waist</th>
<th class="results">Hips</th>
<th class="results">Leg</th>
</tr>
<?php foreach($measurements as $value): ?>
<tr>
<td class="results"><?php echo $value['date']?></td>
<td class="results"><?php echo $value['arm']?></td>
<td class="results"><?php echo $value['waist']?></td>
<td class="results"><?php echo $value['hips']?></td>
<td class="results"><?php echo $value['leg']?></td>
</tr>
<?php endforeach; }?>

</table>

</section>
 
 <?php 
 
 $content = ob_get_clean();
if ($_COOKIE['nOfLogins'] >= 20){
	require __DIR__.'/vipLayout.php';
} else require __DIR__.'/layout.php';
 
 ?>