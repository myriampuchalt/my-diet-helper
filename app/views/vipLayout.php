<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>MyDietHelper</title>
<meta name="description"
	content="Application that helps keeping track of our diet improvements">
<meta name="author" content="Myriam Puchalt Gisep">
<meta name="viewport" content="width=device-width; initial-scale=1.0">

<script type="text/javascript" src="<?php echo '../web/'.Config::$js ?>"> </script>
<link type="text/css" rel="stylesheet"
	href="<?php echo '../web/'.Config::$css ?>" />

</head>

<body onload="checkHeader(); showVipM()">

	<div id="mypage">

		<header id="vip"> 
		<div class="logo">MyDietHelper</div><div class="vip">vip</div>
		<div class="profilePic"><?php if (isset($_SESSION['profilePic'])) echo "<img id='profilePic' src='data:image/jpeg;base64, ".base64_encode($_SESSION['profilePic'])."'/>"?></div>
		<div class="greeting">Hi, <?php if (isset($_SESSION['name'])) echo $_SESSION['name'];?>
                <br> <a href="index.php?ctl=logout">Log Out</a></div>
		</p>
		</header>

		<nav class="vipMenu">

		<ul>
			<li><a href="index.php?ctl=home">Home</a></li>
			<li><a href="index.php?ctl=insertWeight">Weight</a></li>
			<li><a href="index.php?ctl=insertMeasurements">Measurements</a></li>
			<li class="bold"><a href="index.php?ctl=insertWorkout">Workout</a></li>
			<li class="bold"><a href="index.php?ctl=insertImage">My improvement</a></li>
		</ul>
		
		</nav>

		<div id="content">
            
            <?php if(isset($content)) echo $content; else ?>
            
            </div>

		<footer> CEED, DWS 2014-2015 Myriam Puchalt Gisep </footer>
	</div>
</body>
</html>