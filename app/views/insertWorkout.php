<?php

ob_start ();

if (isset ( $params ['message'] )) :
	?>
<b><span style="color: red;"> <?php echo $params['message'] ?></span></b>
<?php endif; ?>

<section class="left">

<h1>Check-In</h1>

<form id='workout' name="insertWorkout" method="POST"
	action="index.php?ctl=insertWorkout">
	<p>
		<label for="date">Date:</label><input type="date" id="date"
			name="date" required="required" value="" />
		<br><br>
		<table>
        <tr>
        <td><label for="name">Exercice Name:</label></td><td><input type="text" id="name" name="name" value=""/></td>
        </tr>
        <tr>
        <td><label for="kcal">Calories Burned:</label></td><td><input type="text" id="kcal" name="kcal" required="required" value=""/></td>
        </tr>
        <tr>
        <td><label for="time">Minutes:</label></td><td><input type="text" id="time" name="time" required="required" value=""/></td>
        </tr>
        
 </table>
 <br>
		<button type="submit" name="insertWorkout">Save Workout</button>
</form>
<br>
<h1>Look for a specific day: </h1>
<form id='workout' name="findWorkout" method="POST"
	action="index.php?ctl=findWorkout">
	<p>
		<label for="date">Date:</label> <input type="date" name="date"><br>
		<button type="submit" name="search">Search</button>

</form>

</section>

<section class="right">
<?php if (isset($params['result'])){

$workouts = $params['result']; ?>

<table>
<tr>
<th class="results">Date</th>
<th class="results">Exercice</th>
<th class="results">Minutes</th>
<th class="results">Calories</th>
</tr>
<?php foreach($workouts as $value): ?>
<tr>
<td class="results"><?php echo $value['date']?></td>
<td class="results"><?php echo $value['name']?></td>
<td class="results"><?php echo $value['time']?></td>
<td class="results"><?php echo $value['kcal']?></td>
</tr>
<?php endforeach; }?>

</table>
</section>
<?php

$content = ob_get_clean ();
	require __DIR__.'/vipLayout.php';
 
 ?>