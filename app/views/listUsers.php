<?php ob_start();

if(isset($params['message'])): ?>
<b><span style="color:red;"> <?php echo $params['message'] ?></span></b>
<?php endif; ?>

<p> Write the id of the user you would like to delete from the list:
 <form id='users' name="users" method="POST" action="index.php?ctl=listUsers">
<input type="text" name="userId" id="userId" />
<button type="submit" name="deleteUsers">Delete User</button>
 </form>
<?php if (isset($params['result'])){
$users = $params['result'];  ?>
 <table>
<tr>
<th>User ID</th>
<th>Username</th>
<th>Password</th>
<th>Email</th>
<th>Age</th>
<th>Weight</th>
<th>Height</th>
<th>Goal Weight</th>
<th>Calories Goal</th>
</tr>
<?php foreach($users as $element): ?>
<tr>
<td><?php echo $element['id']?></td>
<td><?php echo $element['username']?></td>
<td><?php echo $element['password']?></td>
<td><?php echo $element['email']?></td>
<td><?php echo $element['age']?></td>
<td><?php echo $element['weight']?></td>
<td><?php echo $element['height']?></td>
<td><?php echo $element['goalWeight']?></td>
<td><?php echo $element['goalKcal']?></td>
</tr>
<?php endforeach; }?>

</table>
 <?php 
 
 $content = ob_get_clean();
require __DIR__.'/adminLayout.php';
 
 ?>