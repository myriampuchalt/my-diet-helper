<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>MyDietHelper</title>
<meta name="description" content="Application that helps keeping track of our diet improvements">
<meta name="author" content="Myriam Puchalt Gisep">
<meta name="viewport" content="width=device-width; initial-scale=1.0">

<script type="text/javascript" src="<?php echo '../web/'.Config::$js ?>"> </script>
<link type="text/css" rel="stylesheet" href="<?php echo '../web/'.Config::$css ?>" />

</head>

<body onload="checkHeader()">

	<div id="mypage">

		<header id="vip"> 
		<div class="logo">MyDietHelper</div><div class="vip">admin</div>
		<div class="greeting">Hi, <?php if (isset($_SESSION['name'])) echo $_SESSION['name'];?>
                <br> <a href="index.php?ctl=logout">Log Out</a></div>
		</p>
		</header>

		<nav class="adminMenu">

		<ul>

			<li><a href="index.php?ctl=adminHome">Home</a></li>
			<li><a href="index.php?ctl=listUsers">Users</a></li>
			<li><a href="index.php?ctl=insertArticle">Articles</a></li>

		</ul>
		</nav>

		<div id="content">
            
            <?php if(isset($content)) echo $content; else  ?>
            
            </div>

		<footer> CEED, DWS 2014-2015 Myriam Puchalt Gisep </footer>
	</div>
</body>
</html>