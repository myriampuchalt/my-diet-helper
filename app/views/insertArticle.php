<?php ob_start(); ?>


<section class="left">
<?php if(isset($params['message'])): ?>
<b><span style="color:red;"> <?php echo $params['message'] ?></span></b>
<?php endif; ?>
<h1>Insert a new Article</h1>

<form id='article' name="insertArticle" method="POST" action="index.php?ctl=insertArticle">
<p>
 <label for="title">Title:</label><input type="text" id="title" name="title" required="required" value="" />
 <br>
 <label for="content">Content:</label>

<textarea id="content" name="content" rows="20" cols="30" required="required" value=""> </textarea>
<br><br>
<button type="submit" name="insertArticle">Insert Article</button>
 </form>
 </section>
 
 <section class="right">
 
 <p> Write the id of the article you would like to delete from the list:
 <form id='articles' name="articles" method="POST" action="index.php?ctl=deleteArticle">
<input type="text" name="articleId" id="articleId" />
<button type="submit" name="deleteArticle">Delete Article</button>
 </form>
 
 <?php if (isset($params['result'])){
$articles = $params['result']; ?>

<table>
<tr>
<th class="results">Id</th>
<th class="results">Title</th>
<th class="results">Content</th>
</tr>
<?php foreach($articles as $value): ?>
<tr>
<td class="results"><?php echo $value['id']?></td>
<td class="results"><?php echo $value['title']?></td>
<td class="results"><?php echo $value['content']?></td>
</tr>
<?php endforeach; }?>

</table>


</section>

 <?php 
 
 $content = ob_get_clean();
require __DIR__.'/adminLayout.php';
 
 ?>