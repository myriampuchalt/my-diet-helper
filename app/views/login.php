<?php 

//We check if a session has been started
// if ((isset($_SESSION['name'])) && ($_SESSION['name'] !== '')) {
// 	echo "hola";
// 	header("Location: index.php?ctl=home");
// }
// else{ ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>MyDietHelper</title>
<meta name="description"
	content="Application that helps keeping track of our diet improvements">
<meta name="author" content="Myriam Puchalt Gisep">

<meta name="viewport" content="width=device-width; initial-scale=1.0">

<script type="text/javascript" src="<?php echo '../web/'.Config::$js ?>"> </script>

<link type="text/css" rel="stylesheet" href="<?php echo '../web/'.Config::$css ?>" />

</head>

<body id="loginBody">

	<div id="mypage">

		<header> 
		<div class="logo">MyDietHelper</div>
		<form id="login" method="post" action="index.php?ctl=login">
			<br> <label for="username">Username:</label> <input type="text"
				name="username" value="<?php echo $params['username'] ?>" /> <label
				for="password">Password:</label> <input type="password"
				name="password" value="<?php echo $params['password'] ?>" />
			<button type="submit">Log in</button>
		</form>
		
		<?php if(isset($params['message'])): ?>
		<b><span style="color: red;"> <?php echo $params['message'] ?></span></b>
		<?php endif; ?>
		</header>

		<section class="signupform">

		<h1>Complement your diet with mydiethelper</h1>

		<p>The best way to help you keep track of your improvements!</p>
		<p>Not registered yet?
		
		<h1>Sign up </h1></p>
		
		
            <p>It's free!</p>
            
            <form id="signup" method="POST" action="index.php?ctl=signUp">
<br>
<input type="text" id="username" name="username" placeholder="Username" onclick="this.placeholder='';" onblur="this.placeholder=!this.placeholder?'Username':this.placeholder;"/> 
<p>
<input type="password" id="password" name="password" placeholder="Password" onclick="this.placeholder='';" onblur="this.placeholder=!this.placeholder?'Password':this.placeholder;"/>
				<p>
<input type="text" id="email" name="email" placeholder="Email" onclick="this.placeholder='';" onblur="this.placeholder=!this.placeholder?'Email':this.placeholder;"/>
				
				<p>* USERNAME/PASSWORD: Letters/Numbers between 3-15 characters</p>
<button type="submit">Sign Up</button>
 </form>
 
 </section>
 


        </div>

    </body>
</html>
<?php //}?>
