function getParameter(parameter) { 
	  var params = window.location.search.substr(1).split('&');
	 
	  for (var i = 0; i < params.length; i++) {
	    var p=params[i].split('=');
		if (p[0] == parameter) {
		  return decodeURIComponent(p[1]);
		}
	  }
	  return false;
	}

function checkHeader(){
	if (getParameter("showMessage")) {
		alert("Congratulations! You have logged in 5 times!\nAs a thank you you've sent 1 of our fitness articles!");
} 
}

function checkCookie(name){
		  var cookies = "; " + document.cookie;
		  var parts = cookies.split("; " + name + "=");
		  var result = parts.pop().split(";").shift();
		  return result;
}
function showWelcomeM(){
	if (!checkCookie("welcomeMessage")) {
		$welcomeMessage = "Welcome to MyDietHelper! \n With it, you will be able to keep track of your weight and measurements," +
				"making your diet easier to follow! \nREMEMBER: after several Logins, you will be able to unlock our VIP area" +
				" and enjoy or VIP features!";
		if (confirm($welcomeMessage)) document.cookie="welcomeMessage=showed";
	}
}

function showVipM(){
	if (!checkCookie("vipMessage")) {
		$vipMessage = "Congratulations! You have unlocked the VIP area. Now you will be able to enjoy new features, explore them yourself!";
		if (confirm($vipMessage)) document.cookie="vipMessage=showed";
	}
}
