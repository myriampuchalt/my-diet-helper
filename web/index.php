<?php
session_start();

// Front Controller
// We run the Model, the Controller and the Configuration variables

require_once '../app/model.php';
require_once '../app/controller.php';
require_once '../app/adminModel.php';
require_once '../app/adminController.php';
require_once '../app/config.php';


// we create the rutes

$map = array(
		'signUp' => array('controller' => 'Controller', 'action' => 'signUp'),
		'login' => array('controller' => 'Controller', 'action' => 'login'),
		'logout' => array('controller' => 'Controller', 'action' => 'logout'),
		
		'home' => array('controller' => 'Controller', 'action' => 'home'),
		
		'insertPI' => array('controller' => 'Controller', 'action' => 'insertPI'),
		'insertProfilePic' => array('controller' => 'Controller', 'action' => 'insertProfilePic'),
		'insertWeight' => array('controller' => 'Controller', 'action' => 'insertWeight'),
		'insertMeasurements' => array('controller' => 'Controller', 'action' => 'insertMeasurements'),
		'insertWorkout' => array('controller' => 'Controller', 'action' => 'insertWorkout'),
		'insertImage' => array('controller' => 'Controller', 'action' => 'insertImage'),
		
		'getWeight' => array('controller' => 'Controller', 'action' => 'getWeight'),
		'getMeasurements' => array('controller' => 'Controller', 'action' => 'getMeasurements'),
		'getWorkout' => array('controller' => 'Controller', 'action' => 'getWorkout'),
		
		'listWeights' => array('controller' => 'Controller', 'action' => 'listWeights'),
		'listMeasurements' => array('controller' => 'Controller', 'action' => 'listMeasurements'),
		'listWorkouts' => array('controller' => 'Controller', 'action' => 'listWorkouts'),
		
		
		
		'insertArticle' => array('controller' => 'adminController', 'action' => 'insertArticle'),
		'deleteArticle' => array('controller' => 'adminController', 'action' => 'deleteArticle'),
		'listUsers' => array('controller' => 'adminController', 'action' => 'listUsers'),
		'adminHome' => array('controller' => 'adminController', 'action' => 'adminHome'),
);

// We parse the route 

if (isset($_GET['ctl'])){
	if (isset($map[$_GET['ctl']])){
		$rute = $_GET['ctl'];
	} else{
		header('Status: 404 Not Found');
		echo '<html><body><h1>Error 404: The route does not exist <i> </body></html>';
		exit;
	}
} else {
		$rute = 'login';
	}
	
	$controller = $map[$rute];
	
// We run the controller asociated with the selected rute

	if (method_exists($controller['controller'], $controller['action'])){
		call_user_func(array(new $controller['controller'], $controller['action']));
	} else{
		header('Status: 404 Not Found');
		echo '<html><body><h1>Error 404: The route does not exist <i> </body></html>';
	}
		
?>
